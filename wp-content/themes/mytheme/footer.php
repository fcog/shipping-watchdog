    <footer id="footer">
        <div class="container_24">
            <?php
            if (is_active_sidebar('footer-copyright')) :
                dynamic_sidebar('footer-copyright');
            endif;
            ?>   
        </div>
    </footer>

</div>
<?php wp_footer() ?>
</body>
<?php  if (is_home()): ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" charset="utf-8">
    // jQuery(window).load(function() {
        jQuery('#slideshow').flexslider({
            animation: "fade",
            direction: "horizontal",
            slideshowSpeed: 7000,
            animationSpeed: 600,
            directionNav: true,
            initDelay: 0,
            prevText: "",           
            nextText: "",
        });
        jQuery('#headlines-news').flexslider({
            animation: "fade",
            direction: "horizontal",
            slideshowSpeed: 7000,
            animationSpeed: 600,
            directionNav: true,
            initDelay: 0,
            prevText: "",           
            nextText: "",
            slideshow: false,
        });
        jQuery('#multimedia-slider').flexslider({
            animation: "fade",
            direction: "horizontal",
            slideshowSpeed: 7000,
            animationSpeed: 600,
            directionNav: true,
            initDelay: 0,
            prevText: "",           
            nextText: "",
            slideshow: false,
        });             
    // });
</script>    
<?php endif ?>
</html>