<?php get_header(); ?>

    <div id="primary" class="content-area inner">

        <?php if (get_post_type() == 'post' && is_active_sidebar( 'sidebar-blog' )): ?>
            <div id="content" class="site-content has-sidebar col span_6_8" role="main">                            
        <?php else: ?>
            <div id="content" class="site-content" role="main">
        <?php endif ?>

        <?php if ( have_posts() ) : ?>
            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', get_post_type() ); ?>
            <?php endwhile; ?>

        <?php else : ?>
            <?php get_template_part( 'home', '' ); ?>
        <?php endif; ?>

        </div><!-- #content -->
        <?php if (get_post_type() == 'post'): ?>
            <?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
                <div id="secondary" class="widget-area col span_2_8" role="complementary">
                    <?php dynamic_sidebar( 'sidebar-blog' ); ?>
                </div><!-- #secondary -->
            <?php endif; ?> 
        <?php endif ?>        
    </div><!-- #primary -->
</div>
<?php get_footer(); ?>