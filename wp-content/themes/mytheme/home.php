<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="content" class="grid_24">
        <section id="top-section" class="grid_24">
            <div id="slideshow" class="flexslider">
            <?php 
            // Query Arguments
            $args = array(
                'posts_per_page' => 5,
                'category_name' => 'slider',
            );  
     
            // The Query
            $the_query = new WP_Query( $args );

            if ( $the_query->have_posts() ):
            ?>            
                <ul class="slides">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                            <div class="image"><?php the_post_thumbnail('slider'); ?></div>
                            <div class="text">
                                <h2><?php the_title(); ?></h2>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>">Read More</a>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif ?>
            </div>
            <div id="headlines">
                <h3>Headlines</h3>
                    <div id="headlines-news" class="flexslider">
                        <?php
                        $args = array(
                            'posts_per_page' => 12,
                            'category_name' => 'headline',
                        );  

                        // The Query
                        $the_query = new WP_Query( $args );
                        $total = $the_query->post_count;
                        $i = 1;
                        ?>
                        <ul class="slides">
                            <? while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php if ($i==1 || $i==5 || $i==9): ?>
                                    <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 2;">
                                <?php endif ?>
                                        <a class="news" href="<?php the_permalink(); ?>">
                                            <?php if (has_post_thumbnail()) the_post_thumbnail('headline'); ?>
                                            <p><?php the_title(); ?></p>
                                        </a>
                                <?php if ($i==4 || $i==8 || $i==12 || $i==$total): ?>
                                    </li>
                                <?php endif ?>
                            <?php $i++; endwhile ?>
                        </ul>
                    </div>
                <div class="viewall-bar">
                    <a href="">View all news</a>
                </div>
            </div>
        </section>
        <section id="middle-section" class="grid_24">
            <div class="box africa">
                <h3>Africa</h3>
                <a href="africa">View</a>
            </div>
            <div class="box1 americas">
                <h3>Americas</h3>
                <a href="americas">View</a>
            </div>
            <div class="box asia-pacific">
                <h3>Asia-Pacific</h3>
                <a href="asia-pacific">View</a>
            </div>
            <div class="box1 europe">
                <h3>Europe</h3>
                <a href="europe">View</a>
            </div>
            <div class="box gulf">
                <h3>Gulf</h3>
                <a href="gulf">View</a>
            </div>
            <div class="shadow"></div>
        </section>
        <section id="bottom-section" class="grid_24">
            <div id="news">
                <h3>Also in the News</h3>
                <?php
                $args = array(
                    'posts_per_page' => 4,
                    'category_name' => 'uncategorized'
                );  
         
                // The Query
                $the_query = new WP_Query( $args );

                while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>                
                    <article>
                        <?php if (has_post_thumbnail()) the_post_thumbnail('news'); ?>
                        <h4><?php the_title(); ?></h4>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" class="readmore">Read More</a>
                    </article>
                <?php endwhile ?>  
                <div class="readmore-bar">
                    <a href="#">View all news</a>
                </div>
            </div>
            <aside>
                <div id="multimedia">
                    <h3>Multimedia</h3>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'multimedia',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );
                    ?>
                    <div id="multimedia-slider" class="flexslider">
                        <ul class="slides">
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            
                                $video = get_post_custom_values('video'); ?>

                                <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">

                                <?php if ($video[0] != ''): ?> 
                                    <a href="<?php the_permalink(); ?>"><div id="play-button"></div></a>
                                <?php endif ?>
                                <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) the_post_thumbnail('multimedia'); ?>
                                </a>
                                <?php the_excerpt(); ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
                <div id="popular">
                    <h3>Most Popular</h3>
                    <ul>
                        <?php
                        $args = array(
                            'posts_per_page' => 4,
                            'meta_key' => 'post_views_count',
                            'orderby' => 'meta_value_num',
                            'order' => 'DESC',
                        );  
                 
                        // The Query
                        $the_query = new WP_Query( $args );

                        while ( $the_query->have_posts() ) : $the_query->the_post();
                        ?>                      
                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                        <?php endwhile ?>  
                    </ul>
                </div>
                <div class="sidebar-widget">
                   <?php
                    if (is_active_sidebar('homepage-sidebar')) :
                        dynamic_sidebar('homepage-sidebar');
                    endif;
                    ?>
                </div>
            </aside> 
        </section>
        <div id="sitemap" class="grid_24">
            <div id="logo-footer" class="grid_7">
                <img src="<?php echo get_template_directory_uri(); ?>/photos/logo-footer.png">
            </div>
            <div class="sitemap grid_7 prefix_1">
                <?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
            </div>
            <div id="info" class="grid_7 prefix_1">
                <?php
                if (is_active_sidebar('homepage-contact')) :
                    dynamic_sidebar('homepage-contact');
                endif;
                ?>            
                <div id="social-network">
                    <ul>
                        <?php
                        if (is_active_sidebar('homepage-social-buttons')) :
                            dynamic_sidebar('homepage-social-buttons');
                        endif;
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>    
</div> 

<?php get_footer(); ?>    